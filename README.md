# 目的

通过为一段难以维护的祖传代码，补充覆盖完整场景的单元测试，再重构到便于维护的状态，最终能比较容易地添加新功能，从而让程序员学习如何把单元测试融入到日常新功能的开发中。

# 出处

本操练题目有各种编程语言的版本，参见[Emily Bache的该编程操练代码库](https://github.com/emilybache/GildedRose-Refactoring-Kata)。

# 分支

* `master`: 用于介绍本编程操练的目的
* `baseline`: 每次操练都从这个分支创建新的操练分支
* `kata-2021-08-25`: 我在2021年8月25日做的一次操练，使用了class来实现策略模式
* `use-functions-for-strategies`: 使用了function来实现策略模式

# 题目

Hi and welcome to team Gilded Rose. As you know, we are a small inn with a prime location in a prominent city ran by a friendly innkeeper named Allison. We also buy and sell only the finest goods. Unfortunately, our goods are constantly degrading in quality as they approach their sell by date. We have a system in place that updates our inventory for us. It was developed by a no-nonsense type named Leeroy, who has moved on to new adventures. Your task is to add the new feature to our system so that we can begin selling a new category of items. First an introduction to our system:

嗨，欢迎加入镶金玫瑰团队。如您所知，我们是一家位暴风城黄金地段的小旅馆。旅馆老板叫艾莉森，她很友好。除了开旅馆，我们还销售最好的商品。不幸的是，这些商品在接近保质期时，品质会不断下降。我们有一个系统，可以更新库存商品的品质。程序员火车王里诺艾开发完该系统就跑路了。你的任务，是将一项新功能添加到系统中，以便我们可以开始销售新商品。首先介绍一下我们的系统：

* All items have a SellIn value which denotes the number of days we have to sell the item
* 所有商品都有一个 SellIn 值，表示我们必须在这些天内出售该商品

* All items have a Quality value which denotes how valuable the item is
* 所有商品都有一个 Quality 值，表示该商品的价值

* At the end of each day our system lowers both values for every item
* 每过一天，系统会降低每个商品的上述两个值

Pretty simple, right? Well this is where it gets interesting:

很简单，对吧？有趣的还在下面：

* Once the sell by date has passed, Quality degrades twice as fast
* 一旦超过销售期限，品质下降速度将增加一倍

* The Quality of an item is never negative
* 商品的质量永远不会是负值

* “Aged Brie” actually increases in Quality the older it gets
* “陈年干酪”的品质会随着时间推移而提高

* The Quality of an item is never more than 50
* 商品的品质永远不会超过 50

* “Sulfuras”, being a legendary item, never has to be sold or decreases in Quality
* “萨弗拉斯，炎魔拉格纳罗斯之手（炎魔锤）”这种传奇商品，品质不会下降

* “Backstage passes”, like aged brie, increases in Quality as it’s SellIn value approaches; Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but Quality drops to 0 after the concert
* 精英牛头人酋长乐队后台通行证，就像陈年干酪一样，随着其销售期限的临近，价值会提高；当销售期限降到 10 天或以内时，其价值将每日增加 2；降到 5 天或以内时，价值每日增加 3 ；但在音乐会结束后，价值会降为 0。

We have recently signed a supplier of conjured items. This requires an update to our system:

我们最近与一家魔法物品供应商签约。这需要更新我们的系统进行升级：

* “Conjured” items degrade in Quality twice as fast as normal items
* 支持魔法商品，品质下降速度是普通商品的两倍

Feel free to make any changes to the UpdateQuality method and add any new code as long as everything still works correctly. However, do not alter the Item class or Items property as those belong to the goblin in the corner who will insta-rage and one-shot you as he doesn’t believe in shared code ownership (you can make the UpdateQuality method and Items property static if you like, we’ll cover for you).

只要一切能正常工作，可以随意对 update_quality() 方法进行更改，并添加任何新代码。但是，不要更改 Item 类及其属性。因为这些代码属于另一拨程序员。他们反对代码集体所有。
